public class MyDate {
    private int day,month,year;
    private static int[] maxDays={31,28,31,30,31,30,31,31,30,31,30,31};
    public MyDate(int day, int month,int year){
        this.day= day;
        this.month=month-1;
        this.year=year;

    }
    public String toString(){
        return year+"-"+(month<9 ? "0":"")+(month+1)+"-"+(day<10 ?"0":"")+day;
    }
    public void incrementDay(){
        int maxDay=maxDays[month];
        day++;

        if (day> maxDay) {
            if ((month == 1 && inLeapYear())) {
                if (day == 30) {
                    day = 1;
                    incrementMonth(1);
                }
            } else {
                day = 1;
                incrementMonth(1);
            }
        }
    }
    public void incrementMonth(int value){
        int yearIncrement=(month+value)/12;

        month = (month + value)%12;
        if (month<0){
            yearIncrement--;
        }
        month= (month+12)%12;
        year += yearIncrement;

        if (day> maxDays[month]){

            if(month==1 && day==29 && !inLeapYear()) {
                day = 29;
            }else {
                day = maxDays[month];
            }
        }
    }
    public void incrementYear(int value){
        year += value;

        if(month==1 && day==29 && !inLeapYear()) {
            day = 29;
        }

    }
    public void decrementDay(){
        day--;
        if (day==0){
            day=31;
            decrementMonth();

        }
    }
    private boolean inLeapYear(){
        return year%4==0;
    }
    public void decrementMonth(){
        incrementMonth(-1);
    }
    public void decrementYear(){
        incrementYear(-1);

    }
    public  void incrementDay(int value){
        for (int i=0; i<value; i++){
            incrementDay();
        }
    }
    public  void  decrementMonth(int value){
        incrementMonth(-value);
    }

    public void decrementDay(int value) {
        for (int i=0; i<value; i++){
            incrementDay();
        }
    }

    public void decrementYear(int value) {
        incrementYear(-1);
    }

    public void incrementMonth() {
        incrementMonth(1);
    }

    public void incrementYear() {
        incrementYear(1);
    }

    public boolean isBefore(MyDate anotherDate) {
        String str = toString().replace("-","");
        String strAnother = anotherDate.toString().toString();
        System.out.println(str);
        System.out.println(strAnother);
        return Integer.parseInt(str)<Integer.parseInt(strAnother);
    }

    public boolean isAfter(MyDate anotherDate) {
        String str = toString().replace("-","");
        String strAnother = anotherDate.toString().replace("-","");

        return Integer.parseInt(str)<Integer.parseInt(strAnother);
    }

    public int dayDifference(MyDate anotherDate) {
        int count=0;
        MyDate copy= new MyDate(day,month+1,year);
        if (this.isBefore(anotherDate)){

            while (copy.isBefore(anotherDate)){
                count++;
                copy.incrementDay();
            }
        }else if (this.isAfter(anotherDate)){
            while (copy.isAfter(anotherDate)){
                count++;
                copy.decrementDay();
            }
        }
        return count;

    }
}
