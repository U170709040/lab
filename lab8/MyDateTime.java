public class MyDateTime {
    MyDate date;
    MyTime time;

    public MyDateTime(MyDate date, MyTime time) {
        this.date =date;
        this.time= time;
    }

    public String toString(){
        return date.toString()+ " " + time.toString();
    }

    public void incrementDay() {
        date.incrementDay();
    }

    public void incrementHour() {
        incrementHour(1);

    }

    public void incrementHour(int value) {
        int daydiff =time.incrementHour(value); //day difference
        date.incrementDay(daydiff);

    }

    public void decrementHour(int value) {
        int daydiff =time.incrementHour(-value);
        date.decrementDay(-daydiff);

    }

    public void incrementMinute(int value) {
        int daydiff = time.incrementMinute(value);
        date.incrementDay(daydiff);
    }

    public void decrementMinute(int value) {
        int daydiff = time.incrementMinute(-value);
        date.decrementDay(-daydiff);


    }

    public void incrementYear(int value) {
        date.incrementYear(value);
    }

    public void decrementDay() {
        date.decrementDay();
    }

    public void decrementYear() {
        date.decrementYear();
    }

    public void decrementMonth() {
        date.decrementMonth();
    }

    public void incrementDay(int value) {
        date.incrementDay(3);
    }

    public void decrementMonth(int value) {
        date.decrementMonth(value);
    }

    public void decrementDay(int value) {
        date.decrementDay(value);
    }

    public void incrementMonth(int value) {
        date.incrementMonth(value);
    }

    public void decrementYear(int value) {
        date.decrementYear(value);

    }

    public void incrementMonth() {
        date.incrementMonth();
    }

    public void incrementYear() {
        date.incrementYear();
    }

    public boolean isBefore(MyDateTime anotherDateTime) {
        if(date.isBefore(anotherDateTime.date)){
            return true;
        }else if (date.isAfter(anotherDateTime.date)){
            return false;
        }
        return time.isBefore(anotherDateTime.time);
    }

    public boolean isAfter(MyDateTime anotherDateTime) {
        if(date.isAfter(anotherDateTime.date)){
            return true;
        }else if (date.isBefore(anotherDateTime.date)){
            return false;
        }
        return time.isAfter(anotherDateTime.time);
    }

    public String dayTimeDifference(MyDateTime anotherDateTime) {
        int dayDiff= date.dayDifference(anotherDateTime.date);
        dayDiff = date.isAfter(anotherDateTime.date) ? dayDiff: -dayDiff;
        int minDiff = time.minuteDifference(anotherDateTime.time);


        int totalMinuteDifference = Math.abs(24*60*dayDiff + minDiff);

        int day = totalMinuteDifference/ (24*60);
        int hour = (totalMinuteDifference % (24*60))/60;
        int minute = totalMinuteDifference %60;

        return (day>0 ? (day+"day(s)") :"")+  (hour>0 ? (hour+"hour(s)") : "")
                +(minute>0 ? minute+"minute(s)" : "");

    }
}
