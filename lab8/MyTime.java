public class MyTime {
    private int hour;
    private int minute;

    public MyTime(int hour, int minute){
        this.hour =hour;
        this.minute=minute;

    }
    public String toString(){
        return (hour<10 ? "0" :"")+ hour+ ":"+ (minute<10 ? "0": "") +minute;
    }
    public int incrementHour (int value){
        //System.out.println("increment hour value"+value);
        int daydiff =(hour + value)/24;
        //System.out.println("increment hour daydiff"+daydiff);
        //System.out.println("increment hour= "+ hour);
        int newHour=(hour+value)%24;

        if((hour+value)<0){
            daydiff--;
            newHour += 24;
        }
        hour =newHour;
        //System.out.println("increment hour adjusted daydiff"+ daydiff);
        return daydiff;
    }
    public int incrementMinute(int value){
        int hourDiff = (minute+value) /60;
        minute = (minute+value)% 60;
        if (minute+value<0){
            hourDiff--;
            minute+=60;
        }
        return incrementHour(hourDiff);


    }

    public boolean isBefore(MyTime time) {
        return Integer.parseInt(toString().replace(":","")) <
                Integer.parseInt(time.toString().replace(":",""));
    }

    public boolean isAfter(MyTime time) {
        return Integer.parseInt(toString().replace(":","")) <
                Integer.parseInt(time.toString().replace(":",""));
    }

    public int minuteDifference(MyTime time) {
        return (hour * 60 + minute)- (time.hour*60 + time.minute);
    }
}
