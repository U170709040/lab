package drawing.version1;

import java.util.ArrayList;

import shapes.*;

public class Drawing {
	
	private ArrayList<Circle> circles = new ArrayList<Circle>();
	private ArrayList<Rectangle> rectangles = new ArrayList<Rectangle>();
	private ArrayList<Square> squares = new ArrayList<>();
	public double calculateTotalArea(){
		double totalArea = 0;

		for (Circle circle : circles){
			totalArea += circle.area();    
		}
		
		for (Rectangle rect : rectangles){            
			totalArea += rect.area();     
		}

		for (Square s : squares){
			totalArea += s.area();
		}
		return totalArea;
	}
	
	public void addCircle(Circle c) {
		circles.add(c);
	}
	
	public void addRectangle(Rectangle r) {
		rectangles.add(r);
	}
	public void addSquare(Square s) {
		squares.add(s);
	}
}
