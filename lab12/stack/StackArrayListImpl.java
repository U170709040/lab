package stack;
//you can also use already existing class ArrayList
// instead of StackImpl class

import java.util.ArrayList;

public class StackArrayListImpl implements Stack{
    ArrayList<Object> items = new ArrayList<>();

    @Override
    public void push(Object item) {
        items.add(0,item);
    }

    @Override
    public Object pop() {
        return items.remove(0);
    }

    @Override
    public boolean empty() {
        return items.isEmpty();
        //items.size() ==0 ; (you can also write like that)
    }
}
