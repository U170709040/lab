package stack;

public class TestStack {
    public static void main(String[] args){
        Stack stack = new StackArrayListImpl();

        stack.push("A");
        stack.push("B");
        stack.push("C");
        stack.push("D");

        stack.pop();
        stack.pop();
        System.out.println(stack.pop());
        System.out.println(stack.pop());

       /* while(!stack.empty())
            System.out.println(stack.pop());*/
    }
}
