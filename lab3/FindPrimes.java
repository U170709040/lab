public class FindPrimes {
    public  static  void  main(String[] args) {

        //get the number
        int given = Integer.parseInt(args[0]);
        // for each number less than the given
        for (int number =2; number<given; number++){
            boolean isPrime = true;
            //for each divisor less than the number
            for (int divisor =2; divisor<number;divisor++){
                //if number is divisible by divisor
                if (number %divisor==0){
                    isPrime=false;
                    break;
                }
            }
            if(isPrime)
                System.out.print(number+",");
            //print the number


        }




    }


}
