public class Point {


    int xCoord=1;
    int yCoord=1;

    public Point(){
    }
    public Point(int xy){
        xCoord=xy;
        yCoord=xy;
    }

    public Point(int x, int y){
        xCoord=x;
        yCoord=y;

    }
    public  double distance (Point p){
        return Math.sqrt((xCoord-p.xCoord)*(xCoord-p.xCoord)+ (yCoord-p.yCoord)*(yCoord-p.yCoord));
    }
}
