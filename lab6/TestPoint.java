public class TestPoint {
    public static void main(String[] args){
        Point p1= new Point(3,2);
        String str= new String("hello");

        System.out.println("x= "+p1.xCoord+"y= "+p1.yCoord);

        Point p2= new Point(6);
        System.out.println("x= "+p2.xCoord+"y= "+p2.yCoord);
        System.out.println("distance p1 to p2= "+p1.distance(p2));
        System.out.println("distance p2 to p1= "+p2.distance(p1));

        Point p3= new Point();

        System.out.println("x= "+p3.xCoord+"y= "+p3.yCoord);


    }
}
